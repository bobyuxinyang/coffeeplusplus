//
//  NetworkManager.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-25.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface NetworkManager : NSObject
+ (NetworkManager*)sharedManager;
@property (nonatomic, strong) AFHTTPClient *client;
@end
