//
//  shopDetailView.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-5-13.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "ShopDetailView.h"

@implementation ShopDetailView

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"shop_detail_cell.png"]];
    self.frame = CGRectMake(10.0f, 0.0f, 302.0f, 452.0f);
}

- (IBAction)makePhoneCall:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://8004664411"]];
}


- (IBAction)goComment:(id)sender {
        NSLog(@"GoComment");
}

- (IBAction)goLike:(id)sender {
            NSLog(@"Go Like");
}


@end
