//
//  main.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-21.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
