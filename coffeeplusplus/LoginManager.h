//
//  LoginManager.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginManager : NSObject
+ (LoginManager *)sharedLoginManager;

@property (nonatomic, strong) NSString *userId; // coffee user id
@property (nonatomic, strong) NSString *weiboId; // id for weibo
@property (nonatomic, strong) NSString *openId; // id for QQ connect
@end
