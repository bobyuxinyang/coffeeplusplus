//
//  CommonTools.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-16.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "CommonTools.h"


@implementation CommonTools

+ (NSString *)contentForFile:(NSString *)file ofType:(NSString *)type
{
    NSString*filePath=[[NSBundle mainBundle] pathForResource:file ofType:type];
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    return content;
}

+ (AppDelegate *)applicationDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (float)heightOfString:(NSString *)string withWidth:(float)width font:(UIFont *)font
{
    if ([NSNull null] == (id)string) {
        string = @"暂时没有数据";
    }
    CGSize constraintSize = CGSizeMake(width, MAXFLOAT);
    CGSize labelSize = [string sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;
}
@end
