//
//  AboutManyPeopleCoffeeViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-16.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "AboutManyPeopleCoffeeViewController.h"

@interface AboutManyPeopleCoffeeViewController ()

@end

@implementation AboutManyPeopleCoffeeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];   
    UIImageView *nLogoImageView = [[UIImageView alloc] initWithFrame:COFFEE_LOGO_IMAGE_RECT];
    nLogoImageView.image = [UIImage imageNamed:@"about_onecup"];
    nLogoImageView.contentMode = UIViewContentModeCenter;
    nLogoImageView.tag = NAVIGATION_BAR_LOGO_IMAGE_VIEWTAG;
    nLogoImageView.alpha = 1.0f;
    [self.navigationController.navigationBar addSubview:nLogoImageView];

    
    UIImageView *contentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"about_many_content.png"]];
    contentImageView.frame = CGRectMake(0.0f, -50.0f, 320.0f, 480.0f);
    [self.view addSubview:contentImageView];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
