//
//  ShopDetailViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "ShopDetailViewController.h"
#import "StackPanel.h"


@interface ShopDetailViewController ()

@property (strong, nonatomic) UIView *shopDetailPanelView;

@property (strong, nonatomic) IBOutlet PublicBoardView *publicBoardView;

@property (strong, nonatomic) IBOutlet ShopDetailView *shopDetailView;

@end

@implementation ShopDetailViewController

- (void)reloadShopItemDetail
{
    if (self.shopDetailPanelView != nil) {
        [self.shopDetailPanelView removeFromSuperview];
    }
    NSDictionary *shopItem = self.shopItemDetail;
    
    // load public board
    StackPanel *shopDetailPanel = [[StackPanel alloc] initWithFrame:self.view.bounds];
    shopDetailPanel.backgroundColor = [UIColor clearColor];
    
    
    // public board view
//    self.publicBoardView.publicBoardTitle.text = self.shopItemDetail[@"public_board"][@"title"];
    self.publicBoardView.publicBoardContent.text = shopItem[@"public_board"][@"title"];
//    self.publicBoardView.publicBoardContent.text = self.shopItemDetail[@"public_board"][@"content"];
    self.publicBoardView.publicBoardImageView.imageWithURL = [NSURL URLWithString:[BASE_URL stringByAppendingString: shopItem[@"logo_img"]]];
    [shopDetailPanel addStackedView:self.publicBoardView];
    
//
    
    // shop detail view
    
    self.shopDetailView.titleLableView.text = shopItem[@"title"];
    self.shopDetailView.logoImageView.imageWithURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BASE_URL, shopItem[@"logo_img"]]];
    self.shopDetailView.addressTextLableView.text = shopItem[@"address_text"];
    self.shopDetailView.descriptionLableView.text = shopItem[@"description"];
    self.shopDetailView.likeCountLableView.text = [NSString stringWithFormat:@"%d人喜欢", [shopItem[@"like_count"] intValue]];
    self.shopDetailView.commentCountLableView.text = [NSString stringWithFormat:@"%d人评论", [shopItem[@"comment_count"] intValue]];
    
    if ([shopItem[@"my_checkin_count"] intValue] > 0) {
        self.shopDetailView.checkinIconImageView.image = [UIImage imageNamed:@"icon_checked_in.png"];
    } else {
        self.shopDetailView.checkinIconImageView.image = [UIImage imageNamed:@"icon_no_checked_in.png"];
    }
    NSArray *imageList = shopItem[@"view_img_list"];
    CGFloat width = 302.0f;
    NSInteger index = 0;
    for (NSDictionary *imageItem in imageList)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(index * width, 0.0f, width, 208.0f)];
        index++;
        imageView.imageWithURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BASE_URL, imageItem[@"full_img_url"]]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [self.shopDetailView.imageScrollView addSubview:imageView];
    }
    self.shopDetailView.imageScrollView.contentSize = CGSizeMake(index * width, 208.0f);
    
    [shopDetailPanel addStackedView:self.shopDetailView];
    
    
    UIView *emptyView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
    [shopDetailPanel addStackedView:emptyView];
    
    [self.view addSubview:shopDetailPanel];
    
    self.shopDetailPanelView = shopDetailPanel;
}

- (void)fetchShopDetails
{
    NSDictionary *parameters = @{@"shop_id": self.shopItem[@"id"]};
    [[NetworkManager sharedManager].client getPath:API_GET_SHOP_DETAIL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *res = responseObject;
        self.shopItemDetail = res;
        if ([res[@"ret"] isEqualToNumber:@0]) {
            self.shopItemDetail = res[@"shop"];
            NSLog(@"%@", res[@"shop"]);
        }
        [self reloadShopItemDetail];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Bad");
    }];
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [backButton setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    backButton.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *backBtnItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBtnItem;
    
    UIButton *shareButton = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"nav_share.png"] forState:UIControlStateNormal];
    [shareButton setBackgroundImage:[UIImage imageNamed:@"nav_share.png"] forState:UIControlStateHighlighted];
    [shareButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    shareButton.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *shareBtnItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    self.navigationItem.rightBarButtonItem = shareBtnItem;
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    self.navigationItem.title = @"";
    ((UILabel *)[self.navigationController.navigationBar viewWithTag:NAVIGATION_BAR_TITLE_LABEL_VIEWTAG]).text = title;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [UIView beginAnimations:@"" context:nil];
    ((UIView *)[self.navigationController.navigationBar viewWithTag:NAVIGATION_BAR_LOGO_IMAGE_VIEWTAG]).alpha = 0.0f;
    ((UIView *)[self.navigationController.navigationBar viewWithTag:NAVIGATION_BAR_TITLE_LABEL_VIEWTAG]).alpha = 1.0f;
    [UIView commitAnimations];
    
    self.title = self.shopItem[@"title"];
    
    [self fetchShopDetails];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
