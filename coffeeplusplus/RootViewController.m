//
//  RootViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-15.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()
// 用来记录将要打开的URL
@property (nonatomic, strong)   NSURL *toOpen;

- (void)back;
@end

@implementation RootViewController

#pragma mark - private

- (void)back
{
    [self.navigator popViewControllerAnimated:YES];
}

#pragma mark - public

// 登陆回调方法
- (void)delayOpen
{
    [self.navigator openURL:self.toOpen];
}

- (void)didLogout
{
}

#pragma mark - parent

- (void)openedFromViewControllerWithURL:(NSURL *)aUrl
{
    UIButton *navBtn = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [navBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [navBtn setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forState:UIControlStateNormal];
    [navBtn setBackgroundImage:[UIImage imageNamed:@"nav_back.png"] forState:UIControlStateHighlighted];
    navBtn.showsTouchWhenHighlighted = YES;
    UIBarButtonItem *btnItem = [[UIBarButtonItem alloc] initWithCustomView:navBtn];
    self.navigationItem.leftBarButtonItem = btnItem;
}

#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
   
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:20.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = RGBCOLOR(33.0f, 33.0f, 33.0f);
    label.shadowOffset = CGSizeMake(0.0f, 1.0f);
    label.shadowColor = [UIColor whiteColor];
    self.navigationItem.titleView = label;
    label.text = self.params[@"title"];
    [label sizeToFit];
    
    self.navigationItem.titleView = label;
}
@end
