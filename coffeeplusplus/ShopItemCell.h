//
//  ShopItemCell.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-16.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopItemCell : UITableViewCell
@property (strong, nonatomic) NSDictionary *shopItem;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLableView;
@property (weak, nonatomic) IBOutlet UILabel *addressTextLableView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLableView;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLableView;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLableView;
@property (weak, nonatomic) IBOutlet UIImageView *checkinIconImageView;

@end
