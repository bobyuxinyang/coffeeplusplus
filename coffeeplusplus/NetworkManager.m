//
//  NetworkManager.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-25.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "NetworkManager.h"

@implementation NetworkManager
+ (NetworkManager*)sharedManager {
    static NetworkManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[self alloc] init];
    });
    
    return _sharedManager;
}

- (AFHTTPClient *)client
{
    if (_client == nil) {
        _client = [AFHTTPClient clientWithBaseURL:[NSURL URLWithString:BASE_URL]];
        [_client registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [_client setDefaultHeader:@"Accept" value:@"application/json"];
        [_client setParameterEncoding:AFPropertyListParameterEncoding];
    }
    return _client;
}
@end
