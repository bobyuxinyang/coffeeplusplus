//
//  ShopItemCell.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-16.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "ShopItemCell.h"
#import "UIImageView+AFNetworking.h"

@implementation ShopItemCell

- (void)setShopItem:(NSDictionary *)shopItem
{
    if (![shopItem isEqualToDictionary:_shopItem]) {
        _shopItem = shopItem;

        self.titleLableView.text = shopItem[@"title"];
        self.logoImageView.imageWithURL = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", BASE_URL, shopItem[@"logo_img"]]];
        self.addressTextLableView.text = shopItem[@"address_text"];
        self.descriptionLableView.text = shopItem[@"description"];
        self.likeCountLableView.text = [NSString stringWithFormat:@"%d人喜欢", [shopItem[@"like_count"] intValue]];
        self.commentCountLableView.text = [NSString stringWithFormat:@"%d人评论", [shopItem[@"comment_count"] intValue]];
        
        if ([shopItem[@"my_checkin_count"] intValue] > 0) {
            self.checkinIconImageView.image = [UIImage imageNamed:@"icon_checked_in.png"];
        } else {
            self.checkinIconImageView.image = [UIImage imageNamed:@"icon_no_checked_in.png"];
        }
    }
}

@end
