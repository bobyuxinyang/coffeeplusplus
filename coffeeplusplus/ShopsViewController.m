//
//  ShopsViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-15.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "ShopsViewController.h"
#import "ShopItemCell.h"
#import "ShopDetailViewController.h"

#define SHOP_LIST_CELL_HEIGHT 160.0f

@interface ShopsViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *shopList;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic) BOOL isLoaded;
@end

@implementation ShopsViewController

- (void)fetchShopList {
//    if (self.isLoaded) return;
    //[[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"checked_shops"];
    NSDictionary *parameters = nil;
    if ([LoginManager sharedLoginManager].userId != nil) {
        parameters = @{@"userId": [LoginManager sharedLoginManager].userId };
    }
    [[NetworkManager sharedManager].client getPath:API_GET_SHOP_LIST parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *res = responseObject;
        if ([res[@"ret"] isEqualToNumber:@0]) {
            self.shopList = res[@"shop_list"];
//            NSLog(@"%@", self.shopList);
            // 临时更新本地记录的已经checked的shops列表
            NSMutableArray *check_shops = [[NSUserDefaults standardUserDefaults] objectForKey:@"checked_shops"];
            NSMutableArray *shoplist = [NSMutableArray array];
            if (check_shops != nil) {
                for (NSDictionary *shop_item in self.shopList) {
                    NSMutableDictionary *shopitem = [shop_item mutableCopy];
                    if ([check_shops containsObject: [NSString stringWithFormat:@"%@", shop_item[@"id"]]]
                        ) {
                        shopitem[@"my_checkin_count"] = @1;
                    }
                    [shoplist addObject:shopitem];
                }
                self.shopList = shoplist;
            }
            
            [self.tableView reloadData];
            self.isLoaded = YES;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Bad");
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"Shops ViewController Did Load");
    
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];

    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = SHOP_LIST_CELL_HEIGHT;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    // fix tableview size
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                      self.tableView.frame.origin.y,
                                      self.tableView.frame.size.width,
                                      self.tableView.frame.size.height - 44.0f);

    [self.view addSubview:self.tableView];
        
    self.isLoaded = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [UIView beginAnimations:@"" context:nil];
    ((UIView *)[self.navigationController.navigationBar viewWithTag:NAVIGATION_BAR_LOGO_IMAGE_VIEWTAG]).alpha = 1.0f;
    ((UIView *)[self.navigationController.navigationBar viewWithTag:NAVIGATION_BAR_TITLE_LABEL_VIEWTAG]).alpha = 0.0f;
    [UIView commitAnimations];
    
    [self fetchShopList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.shopList count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *shopItem = self.shopList[indexPath.row];
    
    static NSString *CellIdentifier = @"CoffeeShopItemCell";
    ShopItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (ShopItemCell*) [[[NSBundle mainBundle] loadNibNamed:@"ShopItemCell" owner:nil options:nil] lastObject];
        
        UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shop_list_cell_bg"]];
        bgView.contentMode = UIViewContentModeCenter;
        bgView.frame = CGRectMake(18.0f, 0.0f, 302.0f, SHOP_LIST_CELL_HEIGHT);
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundView = bgView;
        cell.textLabel.textColor = RGBCOLOR(187, 187, 187);
        cell.textLabel.highlightedTextColor = RGBCOLOR(187, 187, 187);
    }
    cell.shopItem = shopItem;
    
    //    [tableView selectRowAtIndexPath:self.currentIndex animated:NO scrollPosition:UITableViewRowAnimationTop];
    //    if ([indexPath isEqual:self.currentIndex]) {
    //        cell.textLabel.textColor = [UIColor whiteColor];
    //    }
    //    else {
    //        cell.textLabel.textColor = RGBCOLOR(187, 187, 187);
    //    }
    //    
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *shopItem = self.shopList[indexPath.row];
    ShopDetailViewController *shopDetailVC = [[ShopDetailViewController alloc] initWithNibName:@"ShopDetailViewController" bundle:nil];
    shopDetailVC.shopItem = shopItem;
    [self.navigationController pushViewController:shopDetailVC animated:YES];
}

@end
