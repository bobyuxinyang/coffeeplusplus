//
//  AppDelegate.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-21.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "AppDelegate.h"
#import "SlideNavViewController.h"
#import "ZBarReaderView.h"
#import "LoginViewController.h"


@implementation AppDelegate

- (void)initURLMapping
{
//    *qrcodeScannerNavigator;
//    *aboutManyPeopleCoffeeNavigator;
//    *aboutDookayNavigator;
//    *feedbackNavigator;
    [[UMNavigationController config] setValuesForKeysWithDictionary:[[NSDictionary alloc] initWithObjectsAndKeys:
                                                                     // TODO: rearrange URL Mapping
                                                                     @"ShopsViewController", @"scf://shops",
                                                                     @"QRCodeScannerViewController", @"scf://qrcodescanner",
                                                                     @"AboutManyPeopleCoffeeViewController", @"scf://aboutmanypeoplecoffee",
                                                                     @"AboutDookayViewController", @"scf://aboutdookay",
                                                                     @"FeedbackViewController", @"scf://feedback",
                                                                     nil]];
}

- (void)initNavigators
{
    // TODO: initialize the navigators 1by1
    self.shopsNavigator = [[UMNavigationController alloc] initWithRootViewControllerURL:[[NSURL URLWithString:@"scf://shops"]
                                                                                          addParams:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                                                     nil]]];
    UIButton *nNavBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [nNavBtn setBackgroundImage:[UIImage imageNamed:@"nav_profile.png"] forState:UIControlStateNormal];
    [nNavBtn setBackgroundImage:[UIImage imageNamed:@"nav_profile.png"] forState:UIControlStateHighlighted];
    [nNavBtn addTarget:self.slideNavigator action:@selector(slideButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    nNavBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *nBtnItem = [[UIBarButtonItem alloc] initWithCustomView:nNavBtn];
    self.shopsNavigator.rootViewController.navigationItem.leftBarButtonItem = nBtnItem;
    
    UIButton *nQRCodeBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [nQRCodeBtn setBackgroundImage:[UIImage imageNamed:@"nav_qrcode.png"] forState:UIControlStateNormal];
    [nQRCodeBtn setBackgroundImage:[UIImage imageNamed:@"nav_qrcode.png"] forState:UIControlStateHighlighted];
    [nQRCodeBtn addTarget:self.slideNavigator action:@selector(switchToQRCodeScanNavigator) forControlEvents:UIControlEventTouchUpInside];
    nQRCodeBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *nQRBtnItem = [[UIBarButtonItem alloc] initWithCustomView:nQRCodeBtn];
    self.shopsNavigator.rootViewController.navigationItem.rightBarButtonItem = nQRBtnItem;

    [self.shopsNavigator.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_bg_xx.png"] forBarMetrics:UIBarMetricsDefault];
    self.shopsNavigator.title = @"COFFEE";
    UIImageView *nLogoImageView = [[UIImageView alloc] initWithFrame:COFFEE_LOGO_IMAGE_RECT];
    nLogoImageView.image = [UIImage imageNamed:@"coffee_logo.png"];
    nLogoImageView.contentMode = UIViewContentModeCenter;
    nLogoImageView.tag = NAVIGATION_BAR_LOGO_IMAGE_VIEWTAG;
    nLogoImageView.alpha = 1.0f;
    [self.shopsNavigator.navigationBar addSubview:nLogoImageView];
    UILabel *nTitleView = [[UILabel alloc] initWithFrame:COFFEE_NAVBAR_TITLE_RECT];
    nTitleView.backgroundColor = [UIColor clearColor];
    nTitleView.textColor = RGBCOLOR(105.0f, 52.0f, 0.0f);
    nTitleView.text = @"很多人的咖啡（长宁路店）";
    nTitleView.tag = NAVIGATION_BAR_TITLE_LABEL_VIEWTAG;
    nTitleView.font = [UIFont boldSystemFontOfSize:17.0f];
    nTitleView.alpha = 0.0f;
    [self.shopsNavigator.navigationBar addSubview:nTitleView];
    
    
    self.qrcodeScannerNavigator = [[UINavigationController alloc] initWithRootViewController:[[QRCodeScannerViewController alloc] init]];
    
    // todo: 两个about以及feedback页面还得做
    ////
    self.aboutManyPeopleCoffeeNavigator = [[UMNavigationController alloc] initWithRootViewControllerURL:
                                           [[NSURL URLWithString:@"scf://aboutmanypeoplecoffee"]                                                                                                         addParams:[NSDictionary dictionaryWithObjectsAndKeys:                                                                                                                 nil]]];
    UIButton *a1NavBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [a1NavBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateNormal];
    [a1NavBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateHighlighted];
    [a1NavBtn addTarget:self.aboutManyPeopleCoffeeNavigator.rootViewController action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    a1NavBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *a1BtnItem = [[UIBarButtonItem alloc] initWithCustomView:a1NavBtn];
    self.aboutManyPeopleCoffeeNavigator.rootViewController.navigationItem.rightBarButtonItem = a1BtnItem;
    [self.aboutManyPeopleCoffeeNavigator.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_bg_ox.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.aboutDookayNavigator = [[UMNavigationController alloc] initWithRootViewControllerURL:
                                           [[NSURL URLWithString:@"scf://aboutdookay"]                                                                                                         addParams:[NSDictionary dictionaryWithObjectsAndKeys:                                                                                                                 nil]]];
    UIButton *a2NavBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [a2NavBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateNormal];
    [a2NavBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateHighlighted];
    [a2NavBtn addTarget:self.aboutDookayNavigator.rootViewController action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    a2NavBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *a2BtnItem = [[UIBarButtonItem alloc] initWithCustomView:a2NavBtn];
    self.aboutDookayNavigator.rootViewController.navigationItem.rightBarButtonItem = a2BtnItem;
    [self.aboutDookayNavigator.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_bg_ox.png"] forBarMetrics:UIBarMetricsDefault];
    
    self.feedbackNavigator = [[UMNavigationController alloc] initWithRootViewControllerURL:
                                           [[NSURL URLWithString:@"scf://feedback"]                                                                                                         addParams:[NSDictionary dictionaryWithObjectsAndKeys:                                                                                                                 nil]]];
    UIButton *fNavBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [fNavBtn setBackgroundImage:[UIImage imageNamed:@"nav_profile.png"] forState:UIControlStateNormal];
    [fNavBtn setBackgroundImage:[UIImage imageNamed:@"nav_profile.png"] forState:UIControlStateHighlighted];
    [fNavBtn addTarget:self.feedbackNavigator.rootViewController action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    fNavBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *fBtnItem = [[UIBarButtonItem alloc] initWithCustomView:fNavBtn];
    self.feedbackNavigator.rootViewController.navigationItem.leftBarButtonItem = fBtnItem;
    [self.feedbackNavigator.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_bg_xo.png"] forBarMetrics:UIBarMetricsDefault];

    
}


- (void)initSlideNavigator
{
    self.slideNavigator = [[SlideNavViewController alloc] initWithItems:@[@[self.shopsNavigator]]];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
//    
    [self initURLMapping];
    [self initNavigators];

    [self initSlideNavigator];
    self.window.backgroundColor = [UIColor clearColor];
    self.window.rootViewController = self.slideNavigator;
    [self.window makeKeyAndVisible];
    
    self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    // todo: 判断是否有已经登录过的记录
    [self.slideNavigator presentViewController:self.loginViewController animated:NO completion:nil];
    
    self.sinaWeibo = [[SinaWeibo alloc] initWithAppKey:WEIBO_APP_KEY appSecret:WEIBO_APP_SECRET appRedirectURI:WEIBO_APP_REDIRECT_URI andDelegate:self.loginViewController];
    self.tencentOAuth = [[TencentOAuth alloc] initWithAppId:TENCENT_APP_ID andDelegate:self.loginViewController];
    
    
    [ZBarReaderView class];  
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if ([url.absoluteString hasPrefix:@"sinaweibo"]) {
        return [self.sinaWeibo handleOpenURL:url];
    }
    return [TencentOAuth HandleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    if ([url.absoluteString hasPrefix:@"sinaweibo"]) {
        return [self.sinaWeibo handleOpenURL:url];

    }
    return [TencentOAuth HandleOpenURL:url];
}

@end
