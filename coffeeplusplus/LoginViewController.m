//
//  LoginViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController () 
@property (nonatomic, strong) TencentOAuth *tencentOAuth;
@property (nonatomic, strong) SinaWeibo *sinaWeibo;
@end

@implementation LoginViewController

- (void)gotoShopList
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];
    self.logoImageView.centerY = APP_CONTENT_HEIGHT - 20.0f;
    self.qqLoginButton.centerY = APP_CONTENT_HEIGHT - 80.0f;
    self.weiboLoginButton.centerY = APP_CONTENT_HEIGHT - 135.0f;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // auto login for test
    [self gotoShopList];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sinaweiboLoginClick:(id)sender {
    self.sinaWeibo = [CommonTools applicationDelegate].sinaWeibo;
    [self.sinaWeibo logIn];
}

- (IBAction)qqLoginClick:(id)sender {
    self.tencentOAuth = [CommonTools applicationDelegate].tencentOAuth;
    NSArray *permissions = [NSArray arrayWithObjects:@"get_user_info", @"add_share", nil];
    [self.tencentOAuth authorize:permissions inSafari:NO];
}


#pragma mark -- TencentSessionDelegate
- (void)tencentDidLogin
{
    if (self.tencentOAuth.accessToken && 0 != [self.tencentOAuth.accessToken length])
    {
        [LoginManager sharedLoginManager].openId = self.tencentOAuth.openId;
        // 记录登录用户的OpenID、Token以及过期时间
        NSLog(@"access token: %@", self.tencentOAuth.accessToken);
        NSLog(@"open id: %@", self.tencentOAuth.openId);
        NSLog(@"QQ Login Success");
        // /connect/qq
        NSDictionary *parameters = @{@"openid": self.tencentOAuth.openId,
                                     @"nickname": @"test",
                                     @"figureurl": @"test"};
        [[NetworkManager sharedManager].client postPath:API_CONNECT_QQ parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSDictionary *res = responseObject;
            if ([res[@"ret"] isEqualToNumber:@0]) {
                NSLog(@"%@", res);
                [LoginManager sharedLoginManager].userId = self.tencentOAuth.openId;
                [self gotoShopList];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error: %@", error);
            NSLog(@"Bad");
        }];
    }
    else
    {
        NSLog(@"NO");
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
    NSLog(@"Tencent Did not Login");
}

- (void)tencentDidNotNetWork
{
    NSLog(@"Tencent Did not Network");
}

#pragma mark - SinaWeibo Delegate

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
    [LoginManager sharedLoginManager].weiboId = sinaweibo.userID;
    
    // /connect/sinaweibo
    NSDictionary *parameters = @{@"weibo_id": sinaweibo.userID,
                                 @"nickname": @"test",
                                 @"figureurl": @"test"};
    [[NetworkManager sharedManager].client postPath:API_CONNECT_SINAWEIBO parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *res = responseObject;
        if ([res[@"ret"] isEqualToNumber:@0]) {
//            NSLog(@"Go Login Go");
            [LoginManager sharedLoginManager].userId = sinaweibo.userID;
            [self gotoShopList];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", error);
        NSLog(@"Bad");
    }];
}

- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogOut");
}

- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboLogInDidCancel");
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
{
    NSLog(@"sinaweibo logInDidFailWithError %@", error);
}

- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
{
    NSLog(@"sinaweiboAccessTokenInvalidOrExpired %@", error);
}



@end
