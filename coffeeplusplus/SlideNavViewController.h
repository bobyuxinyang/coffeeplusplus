//
//  SlideNavViewController.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-15.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMSlideNavigationController.h"

@interface SlideNavViewController : UMSlideNavigationController
- (void)switchToQRCodeScanNavigator;
@end
