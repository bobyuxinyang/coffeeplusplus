//
//  AboutDookayViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "AboutDookayViewController.h"

@interface AboutDookayViewController ()

@end

@implementation AboutDookayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];
    UIImageView *contentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"about_dookay.png"]];
    contentImageView.frame = CGRectMake(0.0f, -50.0f, 320.0f, 480.0f);
    [self.view addSubview:contentImageView];
    self.title = @"技术支持";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
