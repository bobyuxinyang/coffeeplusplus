//
//  define.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-25.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#define TENCENT_APP_ID @"100390817"
#define TENCENT_APP_KEY @"2658036a584b7f78f36676d32b99f989"

#define WEIBO_APP_KEY             @"272709498"
#define WEIBO_APP_SECRET          @"18b2a9f024f72efa8fe68731691b4b14"
#define WEIBO_APP_REDIRECT_URI    @"http://weibo.com"

#define BASE_URL @"http://210.14.70.112:822"
#define API_GET_SHOP_LIST BASE_URL@"/shop/list"
#define API_CONNECT_QQ BASE_URL@"/connect/qq"
#define API_CONNECT_SINAWEIBO BASE_URL@"/connect/sinaweibo"
#define API_GET_SHOP_DETAIL BASE_URL@"/shop/detail"
#define API_CREATE_CHECK_IN BASE_URL@"/shop/checkin/new"
//http://210.14.70.112:822/shop/list

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

#define CELL_HEIGHT             44.0f
#define SECTION_HEADER_HEIGHT   29.0f

#define NAVIGATION_BAR_BTN_RECT         CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)
#define COFFEE_LOGO_IMAGE_RECT CGRectMake(0.0f, 0.0f, APP_SCREEN_WIDTH, 44.0f)
#define COFFEE_NAVBAR_TITLE_RECT CGRectMake(48.0f, 0.0f, APP_SCREEN_WIDTH - 96.0f, 44.0f)
#define NAVIGATION_BAR_LOGO_IMAGE_VIEWTAG 1
#define NAVIGATION_BAR_TITLE_LABEL_VIEWTAG 2




#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )  
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 )
#define APP_SCREEN_BOUNDS   [[UIScreen mainScreen] bounds]
#define APP_SCREEN_HEIGHT   (APP_SCREEN_BOUNDS.size.height)
#define APP_SCREEN_WIDTH    (APP_SCREEN_BOUNDS.size.width)
#define APP_STATUS_FRAME    [UIApplication sharedApplication].statusBarFrame
#define APP_CONTENT_WIDTH   (APP_SCREEN_BOUNDS.size.width)
#define APP_CONTENT_HEIGHT  (APP_SCREEN_BOUNDS.size.height-APP_STATUS_FRAME.size.height)


#define SYNTHESIZE_SINGLETON_FOR_CLASS(classname) \
\
static classname *shared##classname = nil; \
\
+ (classname *)shared##classname \
{ \
    @synchronized(self) \
    { \
        if (shared##classname == nil) \
        { \
            shared##classname = [[self alloc] init]; \
        } \
    } \
    \
    return shared##classname; \
} \
\
+ (id)allocWithZone:(NSZone *)zone \
{ \
    @synchronized(self) \
    { \
        if (shared##classname == nil) \
        { \
            shared##classname = [super allocWithZone:zone]; \
            return shared##classname; \
        } \
    } \
    \
    return nil; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
    return self; \
} \

