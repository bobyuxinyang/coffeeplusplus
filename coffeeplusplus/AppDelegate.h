//
//  AppDelegate.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-1-21.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMNavigationController.h"
#import "SlideNavViewController.h"
#import "QRCodeScannerViewController.h"
#import "LoginViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) SinaWeibo *sinaWeibo;
@property (strong, nonatomic) TencentOAuth *tencentOAuth;

@property (strong, nonatomic) UIWindow *window;

- (void)initSlideNavigator;

@property (strong, nonatomic)   SlideNavViewController          *slideNavigator;

@property (strong, nonatomic)   UMNavigationController            *shopsNavigator;
@property (strong, nonatomic)   UINavigationController            *qrcodeScannerNavigator;
@property (strong, nonatomic)   UMNavigationController            *aboutManyPeopleCoffeeNavigator;
@property (strong, nonatomic)   UMNavigationController            *aboutDookayNavigator;
@property (strong, nonatomic)   UMNavigationController            *feedbackNavigator;

@property (strong, nonatomic)   LoginViewController *loginViewController;


@end
