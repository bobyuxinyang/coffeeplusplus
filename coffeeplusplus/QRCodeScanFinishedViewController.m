//
//  QRCodeScanFinishedViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "QRCodeScanFinishedViewController.h"

@interface QRCodeScanFinishedViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *faceImageView;

@end

@implementation QRCodeScanFinishedViewController

- (void)refreshFaceImage
{
    if (self.scanSuccess) {
        self.faceImageView.image = [UIImage imageNamed:@"coffee_succ.png"];
    } else {
        self.faceImageView.image = [UIImage imageNamed:@"coffee_failed.png"];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"content_bg.png"]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self refreshFaceImage];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)finish:(id)sender {

    [self.navigationController dismissViewControllerAnimated:YES completion:^() {
    [self.navigationController popToRootViewControllerAnimated:NO];        
    }];
}
@end
