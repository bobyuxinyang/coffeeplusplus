//
//  RootViewController.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-15.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UMViewController
// 先打开另一个VC（如：登陆），再打开该VC
- (void)delayOpen;
- (void)didLogout;
@end
