//
//  CommonTools.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-16.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@class AppDelegate;

@interface CommonTools : NSObject

+ (NSString *)contentForFile:(NSString *)file ofType:(NSString *)type;
+ (AppDelegate *)applicationDelegate;

+ (float)heightOfString:(NSString *)string withWidth:(float)width font:(UIFont *)font;
@end
