//
//  QRCodeScanFinishedViewController.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRCodeScanFinishedViewController : UIViewController
@property (nonatomic) BOOL scanSuccess;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;
@end
