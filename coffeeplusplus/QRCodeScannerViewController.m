//
//  QRCodeScannerViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "QRCodeScannerViewController.h"
#import "QRCodeScanFinishedViewController.h"


@interface QRCodeScannerViewController () <ZBarReaderDelegate>

@end

@implementation QRCodeScannerViewController

- (void)redrawOverlays
{
    // 中间的框
    UIImageView *scanBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qrcode_scan_bg.png"]];
    scanBgView.frame = CGRectMake(55.0f, 140.0f, 210.0f, 210.0f);
    [self.view addSubview:scanBgView];
    
    UINavigationBar *navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
    UINavigationItem *topItem = [[UINavigationItem alloc] initWithTitle:@"扫描二维码"];
    [navbar pushNavigationItem:topItem animated:NO];
    UIButton *cancelBtn = [[UIButton alloc] initWithFrame:NAVIGATION_BAR_BTN_RECT];
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateNormal];
    [cancelBtn setBackgroundImage:[UIImage imageNamed:@"nav_done.png"] forState:UIControlStateHighlighted];
    [cancelBtn addTarget:self action:@selector(dismissOverlayView) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.showsTouchWhenHighlighted = NO;
    UIBarButtonItem *cancelBtnItem = [[UIBarButtonItem alloc] initWithCustomView:cancelBtn];
    navbar.topItem.rightBarButtonItem = cancelBtnItem;
    [navbar setBackgroundImage:[UIImage imageNamed:@"nav_bar_bg_ox.png"] forBarMetrics:UIBarMetricsDefault];
    
    [self.view addSubview:navbar];
}

- (void)dismissOverlayView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor clearColor];
    self.view.opaque = NO;
    
    self.readerView.frame = CGRectMake(0.0f, 44.0f, APP_SCREEN_WIDTH, APP_SCREEN_HEIGHT - 44.0f);
    self.readerDelegate = self;
    self.scanCrop = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    self.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    // TODO: (optional) additional reader configuration here
    self.showsZBarControls = NO;
    self.wantsFullScreenLayout = NO;
    [self redrawOverlays];
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


-(void)viewDidAppear:(BOOL)animated
{
    // ADD: present a barcode reader that scans from the camera feed
//    ZBarReaderViewController *reader = [[ZBarReaderViewController alloc] init];

    
//    [self presentViewController:reader animated:NO completion:nil];
}

- (void)checkQRCode:(NSString *)qrcode
{
    // Sample QRCode:
    // @"socoffee://1"
    // @"socoffee://3"
 
    NSLog(@"%@", qrcode);
    if ([qrcode length] <= 11) {
        return;
    }
    NSString *shopIdString = [qrcode substringFromIndex:11];
    if (![qrcode hasPrefix:@"socoffee://"] || shopIdString == nil) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"不正确的二维码" message:@"请扫描带有CoffeeApp标志的专属二维码" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    // todo: 为了演示，先更新本地的NSDefault
//    NSDictionary *parameters = @{@"userId": [LoginManager sharedLoginManager].userId,
//                                 @"shop_id": shopIdString};
//    [[NetworkManager sharedManager].client postPath:API_CREATE_CHECK_IN parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *res = responseObject;
//        if ([res[@"ret"] intValue] == 0) {
//            QRCodeScanFinishedViewController *finishVC = [[QRCodeScanFinishedViewController alloc] initWithNibName:@"QRCodeScanFinishedViewController" bundle:nil];
//            BOOL scanSuccess = [res[@"check_in_count"] intValue] == 1;
//            finishVC.scanSuccess = scanSuccess;
//            [self.navigationController pushViewController:finishVC animated:NO];
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Network Error: %@", error);
//    }];
    
    // 临时存在本地，不折腾服务器
    NSMutableArray *check_shops = [[NSUserDefaults standardUserDefaults] objectForKey:@"checked_shops"];
    BOOL scanSuccess = ![check_shops containsObject:shopIdString];
    if (check_shops == nil) {
        check_shops = [NSMutableArray array];
    }
    if (![check_shops containsObject:shopIdString]) {
        [check_shops addObject:shopIdString];
    }
    [[NSUserDefaults standardUserDefaults] setObject:check_shops forKey:@"checked_shops"];
    QRCodeScanFinishedViewController *finishVC = [[QRCodeScanFinishedViewController alloc] initWithNibName:@"QRCodeScanFinishedViewController" bundle:nil];
    finishVC.scanSuccess = scanSuccess;
    [self.navigationController pushViewController:finishVC animated:NO];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for (symbol in results){
        NSString *strFromQRCode = symbol.data;
        [self checkQRCode:strFromQRCode];
        break;
    }



}

@end
