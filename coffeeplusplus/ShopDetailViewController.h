//
//  ShopDetailViewController.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublicBoardView.h"
#import "ShopDetailView.h"

@interface ShopDetailViewController : UIViewController
@property (nonatomic, strong) NSDictionary *shopItem;
@property (nonatomic, strong) NSDictionary *shopItemDetail;
@end
