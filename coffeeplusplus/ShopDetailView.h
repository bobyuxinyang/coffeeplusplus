//
//  shopDetailView.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-5-13.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopDetailView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLableView;
@property (weak, nonatomic) IBOutlet UILabel *addressTextLableView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLableView;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLableView;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLableView;
@property (weak, nonatomic) IBOutlet UIImageView *checkinIconImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;

@end
