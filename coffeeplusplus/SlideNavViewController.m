//
//  SlideNavViewController.m
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-15.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import "SlideNavViewController.h"

#define NAV_ITEMS_DATA @[@{@"title": @"扫一扫", @"hasArrow": @YES},\
                       @{@"title": @"关于One Cup", @"hasArrow": @YES},\
                       @{@"title": @"关于稻壳互联", @"hasArrow": @YES},\
                       @{@"title": @"版本号V1.00", @"hasArrow": @NO},\
                       ]
//                       @{@"title": @"反馈或建议", @"hasArrow": @YES},\

@interface SlideNavViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSArray *navItems;
@end

@implementation SlideNavViewController

- (NSArray *)navItems
{
    if (_navItems == nil) {
        _navItems = NAV_ITEMS_DATA;
    }
    return _navItems;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)switchToQRCodeScanNavigator
{
    UIViewController *qrcodeScanNavigator = [self retrieveViewControllerToShowWithIndex:0];
    [self showModalViewController:qrcodeScanNavigator];
}

- (UIViewController *)retrieveViewControllerToShowWithIndex: (NSInteger)viewIndex
{
    // 0: 扫一扫
    // 1: 关于One Cup
    // 2: 关于稻壳互联
    // 3: 反馈与建议   
    UIViewController *viewControllerToShow;
    if (viewIndex == 0) {
        viewControllerToShow = [CommonTools applicationDelegate].qrcodeScannerNavigator;
    } else if (viewIndex == 1) {
        viewControllerToShow = [CommonTools applicationDelegate].aboutManyPeopleCoffeeNavigator;
    } else if (viewIndex == 2) {
        viewControllerToShow = [CommonTools applicationDelegate].aboutDookayNavigator;
    } else if (viewIndex == 3) {
        viewControllerToShow = [CommonTools applicationDelegate].feedbackNavigator;
    }
    return viewControllerToShow;
}
- (void)showModalViewController:(UIViewController *)vc
{
    [self presentViewController:vc animated:YES completion:nil];
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.navItems count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemData =self.navItems[indexPath.row];
    
    static NSString *CellIdentifier;
    if ([itemData[@"hasArrow"] boolValue]) {
        CellIdentifier = @"UMSlideNavigationControllerSlideWithArrowViewCell";
    } else {
        CellIdentifier = @"UMSlideNavigationControllerSlideViewCell";
    }

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
        UIImageView *bg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_cell_bg.png"]];
        bg.frame = CGRectMake(0.0f, 0.0f, 320.0f, CELL_HEIGHT);
        
        UIImageView *selectedBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_cell_bg_active.png"]];
        selectedBg.frame = CGRectMake(0.0f, 0.0f, 320.0f, CELL_HEIGHT);
        UIImageView *selectedChevron = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_cell_arrow_active.png"]];
        selectedChevron.frame = CGRectMake(235.0f, 14.0f, 15.0f, 15.0f);
        [selectedBg addSubview:selectedChevron];
        cell.selectedBackgroundView = selectedBg;
        
        cell.textLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0.0f, -1.0f);

        if ([itemData[@"hasArrow"] boolValue]) {
            UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nav_cell_arrow.png"]];;
            arrow.frame = CGRectMake(220.0f, 0.0f, 15.0f, 44.0f);
            [bg addSubview:arrow];
        }
        cell.backgroundView = bg;
        cell.textLabel.textColor = RGBCOLOR(187, 187, 187);
        cell.textLabel.highlightedTextColor = RGBCOLOR(187, 187, 187);
    }
    
    cell.textLabel.text = self.navItems[indexPath.row][@"title"];
    
//    [tableView selectRowAtIndexPath:self.currentIndex animated:NO scrollPosition:UITableViewRowAnimationTop];
//    if ([indexPath isEqual:self.currentIndex]) {
//        cell.textLabel.textColor = [UIColor whiteColor];
//    }
//    else {
//        cell.textLabel.textColor = RGBCOLOR(187, 187, 187);
//    }
//    
    return cell;
}



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemData =self.navItems[indexPath.row];
    if (![itemData[@"hasArrow"] boolValue]) {
        // 不选中这一行
        return NULL;
    }
    return indexPath;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    // 回到原来的页面
    [self showItemAtIndex:[NSIndexPath indexPathForRow:0 inSection:0] withAnimation:YES];
    
    NSInteger viewIndex = indexPath.row;
    UIViewController *viewControllerToShow = [self retrieveViewControllerToShowWithIndex: viewIndex];

    [self performSelector:@selector(showModalViewController:) withObject:viewControllerToShow afterDelay:0.5f];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.textLabel.textColor = RGBCOLOR(187, 187, 187);
}

#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.slideView.dataSource = self;
    self.slideView.delegate = self;
    self.slideView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.slideView.backgroundColor = RGBCOLOR(33, 30, 30); // slideNav的背景颜色
    self.slideView.bounces = NO;

    UIButton *logoutBtn = [[UIButton alloc] initWithFrame:CGRectMake(10.0f, APP_SCREEN_HEIGHT - 80.0f, 230.0f, 44.0f)];
    [logoutBtn setTitle:@"注销登录" forState:UIControlStateNormal];
    logoutBtn.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"slide_btn_logout.png"]];
    [self.slideView addSubview:logoutBtn];
    [logoutBtn addTarget:self action:@selector(logoutClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)logoutClicked:(id)sender
{
    [LoginManager sharedLoginManager].userId = nil;
    
    [self presentViewController: [CommonTools applicationDelegate].loginViewController animated:YES completion:^{
        [self showItemAtIndex:[NSIndexPath indexPathForRow:0 inSection:0] withAnimation:YES];
    }];
}

@end
