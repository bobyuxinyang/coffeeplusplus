//
//  PublicBoardView.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-5-13.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicBoardView : UIView
@property (strong, nonatomic) IBOutlet UILabel *publicBoardTitle;
@property (strong, nonatomic) IBOutlet UILabel *publicBoardContent;
@property (strong, nonatomic) IBOutlet UIImageView *publicBoardImageView;
@end
