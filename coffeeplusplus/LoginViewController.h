//
//  LoginViewController.h
//  coffeeplusplus
//
//  Created by YANG Yuxin on 13-3-17.
//  Copyright (c) 2013年 yuxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <TencentSessionDelegate, SinaWeiboDelegate, SinaWeiboRequestDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *weiboLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *qqLoginButton;

@end
